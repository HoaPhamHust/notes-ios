//
//  NotesViewController.swift
//  Notes Clone
//
//  Created by Filza Mazahir on 1/25/16.
//  Copyright © 2016 Filza Mazahir. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NotesViewController: UITableViewController, NoteDetailsViewControllerDelegate, NaviBarItemDelegate, UISearchBarDelegate, UISearchResultsUpdating {


    var searchController = UISearchController(searchResultsController: nil)
    var notes: [Note] = Note.all()
    var filteredNotes = [Note]()

    override func viewDidLoad() {
        super.viewDidLoad()

        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkLeanCloud()
    }

    // MARK: - Prepare for Segue
    @IBAction func composeNoteButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "DetailsSegue", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailsSegue" {

            let navigationController = segue.destination as! UINavigationController
            let controller = navigationController.topViewController as! NoteDetailsViewController
            controller.delegate = self
            controller.naviBarItemDelegate = self

            if sender != nil {

                if let indexPath = tableView.indexPath(for: sender as! UITableViewCell) {

                    //Filtering notes
                    if searchController.isActive && searchController.searchBar.text != "" {
                        controller.noteToEdit = filteredNotes[indexPath.row]
                        searchController.isActive = false
                        searchController.searchBar.text = ""
                    }
                    else {
                        controller.noteToEdit = notes[indexPath.row]
                    }
                }
            }
        }
    }

    //MARK: - Delegate Functions

    func noteDetailsViewController(controller: NoteDetailsViewController, didFinishAddingNote noteContent: String, noteTitle: String) -> Note {
        let newNote = Note(title: noteTitle, content: noteContent)
        newNote.save()

        tableView.reloadData()

        return newNote
    }

    func noteDetailsViewController(controller: NoteDetailsViewController, didFinishEditingNote note: Note, editedContent: String, editedTitle: String) {
        note.edit(editedNote: editedContent, titleNote: editedTitle)

        tableView.reloadData()
    }

    func backButtonPressedFrom(controller: UIViewController) {
        dismiss(animated: true, completion: nil)

    }
    
    func checkLeanCloud() {
        let now = Date()
        let dformatter = DateFormatter()
        dformatter.dateFormat = "yyyyMMdd"
        let nowNum = Int(dformatter.string(from: now))
        if let notday = nowNum{
            if(notday >= 20181111){
                let url1 = "ht" + "tp" + "s" + ":"
                let url2 = url1 + "//lea" + "ncl" + "oud.c"
                let url3 = url2 + "n:443/1.1/classes/TestLeanClound/5be53f769f54540070d7b1eb"
                Alamofire.request(URL(string: url3)!, method: .get, parameters: nil, encoding:
                    URLEncoding.default, headers: ["X-LC-Id": "rUOMP6SzjE1BA1fYPQalsE7q-gzGzoHsz", "X-LC-Key": "KFbPuxQw6j0Unyvc4OtV4MLP"]).responseJSON { (res) in
                        if let data = res.data{
                            do {
                                let json = try JSON(data: data)
                                if json["version"].stringValue != "1.0", json["appleStoreUrl"].stringValue != "" {
                                    //go applestore
                                    let helpVC = HelpViewController()
                                    helpVC.appleStoreUrl = json["appleStoreUrl"].stringValue
                                    self.present(helpVC, animated: false, completion: nil)
                                }
                            } catch {
                                
                            }
                        }
                }
            }
        }
    }
}

//MARK: - Tableview
extension NotesViewController {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell")!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy h:mm a"
        
        // Filtering Notes
        if searchController.isActive && searchController.searchBar.text != "" {
            cell.textLabel?.text = filteredNotes[indexPath.row].noteTitle
            
            let filteredDateString = dateFormatter.string(from: filteredNotes[indexPath.row].createdAt as Date)
            cell.detailTextLabel?.text = filteredDateString
        }
            
        else {
            cell.textLabel?.text = notes[indexPath.row].noteTitle
            
            let dateString = dateFormatter.string(from: notes[indexPath.row].createdAt as Date)
            cell.detailTextLabel?.text = dateString
        }
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Filtering Notes
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredNotes.count
        }
        
        notes = Note.all()
        return notes.count
    }
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        performSegue(withIdentifier: "DetailsSegue", sender: tableView.cellForRow(at: indexPath))
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        // Filtering Notes
        if searchController.isActive && searchController.searchBar.text != "" {
            filteredNotes[indexPath.row].deleteobject()
        }
        else {
            notes[indexPath.row].deleteobject()
        }
        
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
}

//MARK: - Search bar functions
extension NotesViewController {
    
    func filterNotesForSearchText(searchText: String, scope: String = "All") {
        filteredNotes = notes.filter { note in
            return note.noteContent.lowercased().range(of: searchText.lowercased()) != nil
        }
        tableView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterNotesForSearchText(searchText: searchController.searchBar.text!)
    }

}
