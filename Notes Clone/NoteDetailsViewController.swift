//
//  NoteDetailsViewController.swift
//  Notes Clone
//
//  Created by Filza Mazahir on 1/25/16.
//  Copyright © 2016 Filza Mazahir. All rights reserved.
//

import UIKit
class NoteDetailsViewController: UITableViewController, UITextFieldDelegate {
    
    weak var delegate: NoteDetailsViewControllerDelegate?
    weak var naviBarItemDelegate: NaviBarItemDelegate?
    var noteToEdit: Note?
    
    @IBOutlet weak var newNoteTitle: UITextField!
    @IBOutlet weak var newNoteContent: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.newNoteTitle.delegate = self
        self.newNoteContent.delegate = self
        
        if let currentNote = noteToEdit {
            newNoteContent.text = currentNote.noteContent
            newNoteTitle.text = currentNote.noteTitle
        }
 
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let note = noteToEdit {
            delegate?.noteDetailsViewController(controller: self, didFinishEditingNote: note, editedContent: newNoteContent.text!, editedTitle: newNoteTitle.text!)
            
        } else {
            noteToEdit = delegate?.noteDetailsViewController(controller: self, didFinishAddingNote: newNoteContent.text!, noteTitle: newNoteTitle.text!)
            
        }
    }
    
    @IBAction func backBarButtonPressed(_ gsender: Any) {
        naviBarItemDelegate?.backButtonPressedFrom(controller: self)
    }
    

}
