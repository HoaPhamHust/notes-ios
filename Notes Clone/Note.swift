//
//  Note.swift
//  Notes Clone
//
//  Created by Filza Mazahir on 1/25/16.
//  Copyright © 2016 Filza Mazahir. All rights reserved.
//

import Foundation
class Note: NSObject, NSCoding {
    static var key: String = "Notes"
    static var schema: String = "NotesList"
    var noteTitle: String
    var noteContent: String
    var createdAt: NSDate
    var updatedAt: NSDate
    

    init(title: String, content: String) {
        noteContent = content
        noteTitle = title
        createdAt = NSDate()
        updatedAt = NSDate()
    }
    
    // MARK: - NSCoding protocol
    // used for encoding (saving) objects
    func encode(with aCoder: NSCoder) {
        aCoder.encode(noteTitle, forKey: "noteTitle")
        aCoder.encode(noteContent, forKey: "noteContent")
        aCoder.encode(createdAt, forKey: "createdAt")
        aCoder.encode(createdAt, forKey: "updatedAt")
    }
    
    
    // used for decoding (loading) objects
    required init?(coder aDecoder: NSCoder) {
        noteContent = aDecoder.decodeObject(forKey: "noteContent") as! String
        noteTitle = aDecoder.decodeObject(forKey: "noteTitle") as! String
        createdAt = aDecoder.decodeObject(forKey: "createdAt") as! NSDate
        updatedAt = aDecoder.decodeObject(forKey: "updatedAt") as! NSDate
        super.init()
    }
    
    // MARK: - Queries
    static func all() -> [Note] {
        var notes = [Note]()
        let path = Database.dataFilePath(schema: "NotesList")
        if FileManager.default.fileExists(atPath: path) {
            if let data = NSData(contentsOfFile: path) {
                let unarchiver = NSKeyedUnarchiver(forReadingWith: data as Data)
                notes = unarchiver.decodeObject(forKey: Note.key) as! [Note]
                unarchiver.finishDecoding()
            }
        }
        if notes != [] {
            var noteListSorted = false
            while noteListSorted == false {
                noteListSorted = true // reset
                for i in 0..<notes.count-1 {
                    if notes[i].updatedAt.compare(notes[i+1].updatedAt as Date) == ComparisonResult.orderedAscending {
                        let temp = notes[i]
                        notes[i] = notes[i+1]
                        notes[i+1] = temp
                        noteListSorted = false
                    }
                }
            }
        }
        
        return notes
    }
    func save() {
        var notesFromStorage = Note.all()
        var exists = false
        for i in 0..<notesFromStorage.count {
            if notesFromStorage[i].createdAt == self.createdAt {
                notesFromStorage[i] = self
                exists = true
            }
        }
        if !exists {
            notesFromStorage.append(self)
        }
        Database.save(arrayOfObjects: notesFromStorage, toSchema: Note.schema, forKey: Note.key)
    }
    
    func edit(editedNote: String, titleNote: String){
        var notesFromStorage = Note.all()
        for i in 0..<notesFromStorage.count {
            if notesFromStorage[i].createdAt == self.createdAt {
                notesFromStorage[i].noteContent = editedNote
                notesFromStorage[i].updatedAt = NSDate()
                notesFromStorage[i].noteTitle = titleNote
            }
            Database.save(arrayOfObjects: notesFromStorage, toSchema: Note.schema, forKey: Note.key)
        }
    }
    
    func deleteobject() {
        var notesFromStorage = Note.all()
        for i in 0..<notesFromStorage.count {
            if notesFromStorage[i].createdAt == self.createdAt {
                notesFromStorage.remove(at: i)
            }
            Database.save(arrayOfObjects: notesFromStorage, toSchema: Note.schema, forKey: Note.key)
        }
    }
}
